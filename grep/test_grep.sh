#!/bin/bash

SUCCESS=0
FAIL=0
COUNTER=0
DIFF_RES=""

patterns=("test1.txt" ) #"test2.txt" "test3.txt" "test4.txt" "test5.txt"
flags=("e" "i" "v" "c" "l" "n" "h" "o" "s")
pattern_file=("pattern_file.txt")

testing() {
 t=$(echo "$@" | sed "s/VAR/$var/")
 ./s21_grep $t > test_s21_grep.log
    grep $t > test_sys_grep.log
    DIFF_RES="$(diff -s test_s21_grep.log test_sys_grep.log)"
    ((COUNTER++))
    if [ "$DIFF_RES" == "Files test_s21_grep.log and test_sys_grep.log are identical" ]; then
        ((SUCCESS++))
        echo "TEST $COUNTER: OK"
        echo "grep $t"
    else
        ((FAIL++))
        echo "TEST $COUNTER: FAIL $FAIL"
        echo "grep $t"
    fi
    rm test_s21_grep.log test_sys_grep.log
}

for ((i=0; i<=${#flags[@]}-2; i++)); do
    flag1=${flags[i]}
    for pattern in "${patterns[@]}"; do
        testing "${flag1} ${pattern}"
    done
done

for ((i=0; i<=${#flags[@]}-2; i++)); do
    flag1=${flags[i]}
    for ((j=i+1; j<=${#flags[@]}-1; j++)); do
        flag2=${flags[j]}
        for pattern in "${patterns[@]}"; do
            testing "${flag1}${flag2} ${pattern}"
        done
    done
done

# for ((i=0; i<=${#flags[@]}-2; i++)); do
#     flag1=${flags[i]}
#     for ((j=i+1; j<=${#flags[@]}-1; j++)); do
#         flag2=${flags[j]}
#         for ((k=j+1; k<=${#flags[@]}; k++)); do
#             flag3=${flags[k]}
#             for pattern in "${patterns[@]}"; do
#                 testing "${flag1} ${flag2} ${flag3} ${pattern}"
#             done
#         done
#     done
# done

for ((i=0; i<=${#flags[@]}-1; i++)); do
    flag1=${flags[i]}
    for ((j=0; j<=${#flags[@]}-1; j++)); do
        flag2=${flags[j]}
        for ((k=0; k<=${#flags[@]}-1; k++)); do
            flag3=${flags[k]}
            for pattern in "${patterns[@]}"; do
                testing "-f ${pattern_file} ${patterns}"
            done
        done
    done
done

# for ((i=0; i<=${#flags[@]}-1; i++)); do
#     flag1=${flags[i]}
#     for ((j=0; j<=${#flags[@]}-1; j++)); do
#         flag2=${flags[j]}
#         for ((k=0; k<=${#flags[@]}-1; k++)); do
#             flag3=${flags[k]}
#             testing "${flag1} ${flag2} ${flag3}"
#         done
#     done
# done

echo "FAIL: $FAIL"
echo "SUCCESS: $SUCCESS"
echo "ALL: $COUNTER"